create cluster

```
kind create cluster --name my-cluster
```

check available cluster

```
kubectl config get-clusters
```

set current cluster context
```
kubectl config use-context <cluster-name>
```

apply yaml files
```
kubectl apply -f postgres-deployment.yaml
kubectl apply -f postgres-service.yaml
kubectl apply -f pgbouncer-deployment.yaml
kubectl apply -f pgbouncer-service.yaml
```

verify deployment
```
kubectl get deployments
kubectl get pods
kubectl get services
```

create secret for pgbouncer credentials
```
kubectl create secret generic pgbouncer-credentials \
  --from-literal=dbname=<dbname> \
  --from-literal=dbhost=postgres-svc \
  --from-literal=dbuser=<dbuser> \
  --from-literal=dbpass=<dbpass>
```

connect to the PgBouncer Kubernetes Service from your Mac, you can use port-forwarding as you mentioned:
```
kubectl port-forward svc/pgbouncer-svc 6432:6432
```